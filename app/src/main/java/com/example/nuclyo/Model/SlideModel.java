package com.example.nuclyo.Model;

public class SlideModel {
    private String title;
    private String msg;
    private int gradientColor;


    public SlideModel(String title, String msg, int gradientColor) {
        this.title = title;
        this.msg = msg;
        this.gradientColor = gradientColor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getGradientColor() {
        return gradientColor;
    }

    public void setGradientColor(int gradientColor) {
        this.gradientColor = gradientColor;
    }
}
