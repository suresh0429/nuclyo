package com.example.nuclyo.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.nuclyo.Model.SlideModel;
import com.example.nuclyo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SPECBEE on 8/4/2017.
 */

public class SliderAdapter extends PagerAdapter {
    private Context context;
    private List<SlideModel> slideModelList ;

    public SliderAdapter(Context context, List<SlideModel> slideModelList) {
        this.context = context;
        this.slideModelList = slideModelList;
    }

    @Override
    public int getCount() {
        return slideModelList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_slider, null);

        TextView textView = (TextView) view.findViewById(R.id.textTitle);
        RelativeLayout linearLayout = (RelativeLayout) view.findViewById(R.id.linearLayout);
        ImageView imageView = (ImageView) view.findViewById(R.id.img);
        imageView.setImageResource(slideModelList.get(position).getGradientColor());

        textView.setText(slideModelList.get(position).getTitle());
        //linearLayout.setBackgroundColor(slideModelList.get(position).getGradientColor());

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
