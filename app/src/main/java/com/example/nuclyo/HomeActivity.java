package com.example.nuclyo;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nuclyo.Adapters.HomeAdapter;
import com.example.nuclyo.Adapters.SliderAdapter;
import com.example.nuclyo.Model.HomeModel;
import com.example.nuclyo.Model.SlideModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends Activity {
    List<HomeModel> homeModelList = new ArrayList<>();
    List<SlideModel> slideModelList = new ArrayList<>();

    SliderAdapter sliderAdapter;


    @BindView(R.id.ic_back)
    ImageView icBack;
    @BindView(R.id.ic_title)
    TextView icTitle;
    @BindView(R.id.ic_drop)
    ImageView icDrop;
    @BindView(R.id.ic_notifications)
    ImageView icNotifications;
    @BindView(R.id.ic_menu)
    ImageView icMenu;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    TabLayout indicator;
    @BindView(R.id.recycler)
    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        HomeAdapter adapter = new HomeAdapter(getApplicationContext(), homeModelList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recycler.setLayoutManager(mLayoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(adapter);


        sliderAdapter = new SliderAdapter(this,slideModelList);
        viewPager.setAdapter(sliderAdapter);
        indicator.setupWithViewPager(viewPager, true);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);

        homeMenuData();
        slideData();
    }

    private void slideData() {
        SlideModel slideModel = new SlideModel("Electricity Bill","Lorem ipsum is placeholder text commonly used in the graphic, print",R.drawable.gradient_color5);
        slideModelList.add(slideModel);

        slideModel = new SlideModel("Electricity Bill","Lorem ipsum is placeholder text commonly used in the graphic, print",R.drawable.gradient_color1);
        slideModelList.add(slideModel);

        slideModel = new SlideModel("Electricity Bill","Lorem ipsum is placeholder text commonly used in the graphic, print",R.drawable.gradient_color2);
        slideModelList.add(slideModel);

        sliderAdapter.notifyDataSetChanged();

    }

    private void homeMenuData() {
        HomeModel model = new HomeModel("Events", R.drawable.ic_events);
        homeModelList.add(model);

        model = new HomeModel("All bills", R.drawable.ic_allbills);
        homeModelList.add(model);

        model = new HomeModel("Tickets", R.drawable.ic_tickets);
        homeModelList.add(model);

        model = new HomeModel("Vehicle Registration", R.drawable.ic_vehicleregistration);
        homeModelList.add(model);

        model = new HomeModel("Notice Board", R.drawable.ic_noticeboard);
        homeModelList.add(model);

        model = new HomeModel("Help", R.drawable.ic_help);
        homeModelList.add(model);

    }

    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            HomeActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() < slideModelList.size() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }
}
